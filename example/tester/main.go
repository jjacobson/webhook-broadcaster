package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	for i := 0; i < 3; i++ {
		path := fmt.Sprintf("/notify%d", i)
		http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				w.WriteHeader(500)
				w.Write([]byte("{\"message\": \"Error reading request body.\"}"))
				log.Printf("Error reading request body: %s\n", err.Error())
				return
			}

			log.Printf("[%s]: Received message: %s\n", path, body)

			w.WriteHeader(200)
			w.Write([]byte("{\"message\": \"Received.\"}"))
		})
	}

	log.Printf("Listening on port 8080\n")
	panic(http.ListenAndServe(":8080", nil))
}
