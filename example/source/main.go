package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func main() {
	secretKey := os.Getenv("SECRET_KEY")
	if secretKey == "" {
		panic("No secret key.")
	}

	url := fmt.Sprintf("http://webhook-broadcaster:8080/?key=%s", secretKey)

	for {
		// wait 5 seconds
		time.Sleep(5 * time.Second)

		// post the test
		body := []byte(fmt.Sprintf(`{"message": %d}`, rand.Uint64()))
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(body))
		if err != nil {
			log.Printf("Error: %s\n", err.Error())
		} else {
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			log.Printf("Response: %s\n", string(body))
		}
	}
}
