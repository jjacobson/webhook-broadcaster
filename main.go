package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func getDestinations() []*url.URL {
	rawString := os.Getenv("DESTINATIONS")
	if rawString == "" {
		log.Println("No destinations. Please set the `DESTINATIONS` environment variable.")
		os.Exit(1)
	}

	lines := strings.Split(rawString, "\n")
	urls := []*url.URL{}
	for _, line := range lines {
		if line == "" {
			continue
		}

		// validate the url
		u, err := url.Parse(line)
		if err != nil {
			log.Printf("Error parsing URL: %s\n", err.Error())
			os.Exit(1)
		}

		urls = append(urls, u)
	}

	return urls
}

func sendRequest(dest string, body []byte, header http.Header) {
	client := &http.Client{}

	// make a new request with the body
	req, err := http.NewRequest("POST", dest, bytes.NewBuffer(body))
	if err != nil {
		log.Printf("Error sending request to %s: %s\n", dest, err.Error())
		return
	}

	// copy the headers
	for k, v := range header {
		for _, x := range v {
			req.Header.Set(k, x)
		}
	}

	if _, err := client.Do(req); err != nil {
		log.Printf("Error sending request to %s: %s\n", dest, err.Error())
	} else {
		log.Printf("Sent request to %s\n", dest)
	}
}

func main() {
	secretKey := os.Getenv("SECRET_KEY")
	if secretKey == "" {
		log.Println("No secret key. Please set the `SECRET_KEY` environment variable.")
		os.Exit(1)
	}

	destinations := getDestinations()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// get key
		query := r.URL.Query()
		key := query.Get("key")
		if key == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("{\"message\": \"`key` is required as a URL query parameter\"}"))
			return
		}

		// validate key
		if key != secretKey {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("{\"message\": \"Invalid `key`.\"}"))
			return
		}

		// read the body
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("{\"message\": \"Error reading request body.\"}"))
			log.Printf("Error reading request body: %s\n", err.Error())
			return
		}

		// send request to destinations
		for _, dest := range destinations {
			go sendRequest(dest.String(), body, r.Header)
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("{\"message\": \"Sent to destinations.\"}"))
	})

	log.Println("Destinations:")
	for _, dest := range destinations {
		log.Printf("  %s\n", dest.String())
	}
	log.Printf("Listening on port 8080\n")
	panic(http.ListenAndServe(":8080", nil))
}
