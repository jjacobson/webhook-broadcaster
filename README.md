# webhook-broadcaster

Forwards any requests using the secret key to the destinations provided in the `DESTINATIONS` environment variable. Useful for receiving webhooks without having to register multiple destinations with the source.

## Environment Variables

Name              | Required | Description
------------------|----------|-------------------------
SECRET_KEY        | Yes      | Used to verify the webhook requests. It is recommended from a security standpoint to rotate this key every so often.
DESTINATIONS      | Yes      | A list of destinations to forward requests to. This is a multiline value where each destination is on its own line.

## Limitations
  - Only forwards the request headers and body
  - Does not allow for self-signed certs. Could be achieved in the future using a `CERT` environment variable or using a volume.
  - No retrying. If a destination fails to receive a message, the broadcaster does not try again.
  - No persistence. If the service is in the middle of send requests and dies, those requests are lost.

## License
MIT
